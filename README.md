# tacon-snippet

This project is not intended to be used as a library.

It's just a collection of classes to copy/paste wherever you need them.

[tacon.dev](https://www.tacon.dev)

[Azserve s.r.l.](https://www.azserve.com)
