package dev.tacon.snippet.image;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import javax.imageio.ImageIO;

public class ImageResizer {

	private final static Logger LOGGER = System.getLogger(ImageResizer.class.getName());

	public enum ResizeMethod {
		WARP,
		CUT,
		BAND
	}

	public static BufferedImage getImage(final InputStream input, final ResizeMethod resizeMethod, final int width, final int height) throws IOException {
		return getImage(input, resizeMethod, Integer.valueOf(width), Integer.valueOf(height));
	}

	public static BufferedImage getImage(final InputStream input, final ResizeMethod resizeMethod, final Integer width, final Integer height) throws IOException {

		final BufferedImage originalImage = ImageIO.read(input);
		final int originalWidth = originalImage.getWidth();
		final int originalHeight = originalImage.getHeight();
		final int finalWidth = width == null ? originalWidth : width.intValue();
		final int finalHeight = height == null ? originalImage.getHeight() : height.intValue();

		LOGGER.log(Level.INFO, "Convert " + originalWidth + " x " + originalHeight + " to " + finalWidth + " x " + finalHeight + " method: " + resizeMethod);

		if (originalHeight == finalHeight && originalWidth == finalWidth) {
			return originalImage;
		}

		final double originalRatio = (double) originalWidth / originalHeight;
		final double finalRatio = (double) finalWidth / finalHeight;
		LOGGER.log(Level.DEBUG, "originalRatio:" + originalRatio + " finalRatio:" + finalRatio);
		if (resizeMethod == ResizeMethod.WARP || originalRatio == finalRatio) {
			LOGGER.log(Level.DEBUG, "WARP");
			return scale(finalWidth, finalHeight, originalImage);
		} else if (resizeMethod == ResizeMethod.CUT) {
			if (originalRatio > finalRatio) {
				// i.e. 300x200 (1.5) => 100x100 (1)
				final int w = (int) (finalHeight * originalRatio);
				LOGGER.log(Level.DEBUG, "CUT scale to " + w + " x " + finalHeight);
				final BufferedImage scaledImage = scale(w, finalHeight, originalImage);
				LOGGER.log(Level.DEBUG, "CUT reduce to " + finalWidth + " x " + finalHeight);
				return scaledImage.getSubimage((w - finalWidth) / 2, 0, finalWidth, finalHeight);
			}
			// i.e. 300x200 (1.5) => 200x100 (2)
			final int h = (int) (finalWidth / originalRatio);
			LOGGER.log(Level.DEBUG, "CUT scale to " + finalWidth + " x " + h);
			final BufferedImage scaledImage = scale(finalWidth, h, originalImage);
			LOGGER.log(Level.DEBUG, "CUT reduce to " + finalWidth + " x " + finalHeight);
			return scaledImage.getSubimage(0, (h - finalHeight) / 2, finalWidth, finalHeight);
		} else if (resizeMethod == ResizeMethod.BAND) {
			if (originalRatio > finalRatio) {
				// i.e. 300x200 (1.5) => 100x100 (1)
				final int h = (int) (finalWidth / originalRatio);
				LOGGER.log(Level.DEBUG, "BAND scale to " + finalWidth + " x " + h);
				final BufferedImage scaledImage = scale(finalWidth, h, originalImage);
				final BufferedImage imageBuff = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_4BYTE_ABGR); // need alpha channel
				LOGGER.log(Level.DEBUG, "BAND expand to " + finalWidth + " x " + finalHeight);
				imageBuff.getGraphics().drawImage(scaledImage, 0, (finalHeight - h) / 2, finalWidth, h, new Color(0, 0, 0, 0), null);
				return imageBuff;
			}
			// i.e. 300x200 (1.5) => 200x100 (2)
			final int w = (int) (finalHeight * originalRatio);
			LOGGER.log(Level.DEBUG, "BAND scale to " + w + " x " + finalHeight);
			final BufferedImage scaledImage = scale(w, finalHeight, originalImage);
			final BufferedImage imageBuff = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_4BYTE_ABGR); // need alpha channel
			LOGGER.log(Level.DEBUG, "BAND expand to " + finalWidth + " x " + finalHeight);
			imageBuff.getGraphics().drawImage(scaledImage, (finalWidth - w) / 2, 0, w, finalHeight, new Color(0, 0, 0, 0), null);
			return imageBuff;
		}

		return null;
	}

	public static BufferedImage scale(final int width, final int height, final BufferedImage originalImage) {
		final Image scaledImage = originalImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		final BufferedImage imageBuff = new BufferedImage(width, height, originalImage.getType());
		imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0, 0), null);
		return imageBuff;
	}
}
