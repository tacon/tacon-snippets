package dev.tacon.snippet.image;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dev.tacon.snippet.image.ImageResizer.ResizeMethod;

public class ImageResizerTest {

	private static final String IMAGE = "/image/image.png";
	private static final String TEST_OUTPUT = "test-output";
	private final static int TEST_IMAGE_WIDTH = 300;
	private final static int TEST_IMAGE_HEIGHT = 200;

	@BeforeAll
	static void checkInit() throws IOException {
		try (InputStream input = ImageResizerTest.class.getResourceAsStream(IMAGE)) {
			final BufferedImage testImage = ImageIO.read(input);
			assertEquals(TEST_IMAGE_WIDTH, testImage.getWidth());
			assertEquals(TEST_IMAGE_HEIGHT, testImage.getHeight());
		}
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(TEST_OUTPUT))) {
			for (final Path path : stream) {
				if (path.getFileName().toString().endsWith(".png")) {
					Files.delete(path);
				}
			}
		}
	}

	@Test
	void noResize() throws IOException {
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT);
	}

	@Test
	void warp() throws IOException {
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_WIDTH / 2); // square
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 4);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT * 2);
		test(ResizeMethod.WARP, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT * 4);
	}

	@Test
	void cut() throws IOException {
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_WIDTH / 2); // square
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 4);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT * 2);
		test(ResizeMethod.CUT, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT * 4);
	}

	@Test
	void band() throws IOException {
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_WIDTH / 2); // square
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 2);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH / 2, TEST_IMAGE_HEIGHT / 4);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH, TEST_IMAGE_HEIGHT * 2);
		test(ResizeMethod.BAND, TEST_IMAGE_WIDTH * 2, TEST_IMAGE_HEIGHT * 4);
	}

	static void test(final ResizeMethod resizeMethod, final int width, final int lenght) throws IOException {

		try (InputStream is = ImageResizerTest.class.getResourceAsStream(IMAGE)) {
			final BufferedImage image = ImageResizer.getImage(is, resizeMethod, width, lenght);
			assertEquals(width, image.getWidth());
			assertEquals(lenght, image.getHeight());
			ImageIO.write(image, "png", new File("test-output/image-" + resizeMethod + "-" + width + "x" + lenght + ".png"));
		}
	}
}
